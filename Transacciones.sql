USE [adam]
GO
/****** Object:  StoredProcedure [dbo].[AgrupacionesHistorico]    Script Date: 01/03/2016 12:09:51 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[AgrupacionesHistorico]
(
@Trabajador nvarchar(10)
)
AS
BEGIN

DECLARE @longitudT as int,
		@i as int,
		@fechaalta as datetime,
		@fechabaja as datetime,
		@causabaja as char(10)

SET @longitudT = LEN(@Trabajador)
SET @i = 0

WHILE @i < @longitudT
BEGIN
	SET @Trabajador = ' ' + @Trabajador
	SET @i = @i + 1
END

delete from hist_agrupaciones where trabajador = @Trabajador

SET @fechabaja = (select fecha_baja from trabajadores_grales where trabajador = @Trabajador)
SET @causabaja = (select causa_baja from trabajadores_grales where trabajador = @Trabajador)
SET @fechaalta = (select fecha_ingreso from trabajadores_grales where trabajador = @Trabajador)

insert into hist_agrupaciones 
select DISTINCT compania, agrupacion, dato, trabajador, @fechaalta, 1,
@fechabaja, @causabaja, 1 from rel_trab_agr where trabajador = @Trabajador
--and agrupacion <> 'C COSTOS'
and agrupacion <> 'DEC03'
and agrupacion <> 'FAB PASMOL'

select * from hist_agrupaciones where trabajador = @Trabajador

END

--EXEC AgrupacionesHistorico '26525'

--select * from rel_trab_agr where trabajador = '     24169'
select * from hist_agrupaciones where trabajador = '     24169'

select * from transacciones_ptes


SELECT * FROM calendario_procesos WHERE tipo_nomina = 'B1' and anio = 2016


select * from calendario_procesos where tipo_nomina = 'B3' AND anio = 2016

SELECT * FROM cal_proc_cias WHERE COMPANIA = 'COME'

select * from cal_proc_cias where id_calendario = 20054 and compania = 'COME'

--update cal_proc_cias set sit_periodo = 1 where id_calendario = 20054 and compania = 'COME'