-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jose Antonio Escobar Macias>
-- Create date: <16/11/2016>
-- Description:	<Store procedure para habilitar temporalmente un rango de   trabajadores >
-- =============================================
ALTER PROCEDURE HabilitarTrabajadores
	-- Add the parameters for the stored procedure here
	(@habilitar as int)
AS
BEGIN
DECLARE @longitudT		as int,    
		@i				as int,
		@trabajador2	as nvarchar(10),
		@contador		as int,
		@compania		as char(4),
		@id int, 
		@trabajador varchar(20)
		

	--Creo una tabla temporal
	CREATE TABLE #TrabajadorAModificar(id int,noTrabajador nvarchar(10))

	--Inserto los datos en la tabla temporal
	BULK
	INSERT #TrabajadorAModificar FROM '\\SRV15-ASP4\ADAM\trabajadores.txt'--Ruta archivo
	WITH
	( FIELDTERMINATOR = ',', --separa campos
	  ROWTERMINATOR = '\n' ) --separa filas

	--ciclo para que por cada tupla haga algo
	if @habilitar = 1 
		select top 1 @id=id , @trabajador = noTrabajador  from #TrabajadorAModificar

		while (@@rowcount > 0)
		begin

		  print @trabajador
		  SET @longitudT = LEN(@trabajador)  SET @i = @longitudT WHILE @i < 10 BEGIN   
		  SET @trabajador = ' ' + @trabajador   SET @i = @i + 1  END
		  update trabajadores_grales set sit_trabajador  = 1 where trabajador  = @trabajador
		  select trabajador,sit_trabajador from trabajadores_grales where trabajador  = @trabajador


		  delete from #TrabajadorAModificar where id=@id
		  select top 1 @id=id , @trabajador = noTrabajador  from #TrabajadorAModificar
		end

	if @habilitar = 2
		select top 1 @id=id , @trabajador = noTrabajador  from #TrabajadorAModificar

		while (@@rowcount > 0)
		begin

		  print @trabajador
		  SET @longitudT = LEN(@trabajador)  SET @i = @longitudT WHILE @i < 10 BEGIN   
		  SET @trabajador = ' ' + @trabajador   SET @i = @i + 1  END
		  update trabajadores_grales set sit_trabajador  = 2 where trabajador  = @trabajador
		  select  trabajador,sit_trabajador from trabajadores_grales where trabajador  = @trabajador


		  delete from #TrabajadorAModificar where id=@id
		  select top 1 @id=id , @trabajador = noTrabajador  from #TrabajadorAModificar
		end


END
GO

exec  HabilitarTrabajadores 1


exec  HabilitarTrabajadores 2


 update trabajadores_grales set sit_trabajador  = 2 where trabajador  = 
   update trabajadores_grales set sit_trabajador  = 1 where trabajador  = @trabajador





