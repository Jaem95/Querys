USE [Tarificador]
GO
/****** Object:  StoredProcedure [dbo].[SubirInformacion]    Script Date: 08/06/2016 9:34:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Paola Viquez>
-- Create date: <01 Jun 2016>
-- Description:	<Lee y actualiza informacion de tarificador Cisco>
-- =============================================
ALTER PROCEDURE [dbo].[SubirInformacion]
	-- Add the parameters for the stored procedure here
	@Direccion nvarchar(max)
	--Poner aqui nombre de archivo
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	

    -- Insert statements for procedure here
	CREATE TABLE #Temp
	( 
		cdrRecordType									varchar(max),
		globalCallID_callManagerId						varchar(max),
		globalCallID_callId								varchar(max),
		origLegCallIdentifier							varchar(max),
		dateTimeOrigination								varchar(max),
		origNodeId										varchar(max),
		origSpan										varchar(max),
		origIpAddr										varchar(max),
		callingPartyNumber								varchar(max),
		callingPartyUnicodeLoginUserID					varchar(max),
		origCause_location								varchar(max),
		origCause_value									varchar(max),
		origPrecedenceLevel								varchar(max),
		origMediaTransportAddress_IP					varchar(max),
		origMediaTransportAddress_Port					varchar(max),
		origMediaCap_payloadCapability					varchar(max),
		origMediaCap_maxFramesPerPacket					varchar(max),
		origMediaCap_g723BitRate						varchar(max),
		origVideoCap_Codec								varchar(max),
		origVideoCap_Bandwidth							varchar(max),
		origVideoCap_Resolution							varchar(max),
		origVideoTransportAddress_IP					varchar(max),
		origVideoTransportAddress_Port					varchar(max),
		origRSVPAudioStat								varchar(max),
		origRSVPVideoStat								varchar(max),
		destLegIdentifier								varchar(max),
		destNodeId										varchar(max),
		destSpan										varchar(max),
		destIpAddr										varchar(max),
		originalCalledPartyNumber						varchar(max),
		finalCalledPartyNumber							varchar(max),
		finalCalledPartyUnicodeLoginUserID				varchar(max),
		destCause_location								varchar(max),
		destCause_value									varchar(max),
		destPrecedenceLevel								varchar(max),	
		destMediaTransportAddress_IP					varchar(max),	
		destMediaTransportAddress_Port					varchar(max),
		destMediaCap_payloadCapability					varchar(max),
		destMediaCap_maxFramesPerPacket					varchar(max),
		destMediaCap_g723BitRate						varchar(max),
		destVideoCap_Codec								varchar(max),	
		destVideoCap_Bandwidth							varchar(max),
		destVideoCap_Resolution							varchar(max),
		destVideoTransportAddress_IP					varchar(max),
		destVideoTransportAddress_Port					varchar(max),
		destRSVPAudioStat								varchar(max),
		destRSVPVideoStat								varchar(max),
		dateTimeConnect									varchar(max),
		dateTimeDisconnect								varchar(max),	
		lastRedirectDn									varchar(max),
		pkid											varchar(max),
		originalCalledPartyNumberPartition				varchar(max),
		callingPartyNumberPartition						varchar(max),
		finalCalledPartyNumberPartition					varchar(max),
		lastRedirectDnPartition							varchar(max),
		duration										varchar(max),
		origDeviceName									varchar(max),
		destDeviceName									varchar(max),
		origCallTerminationOnBehalfOf					varchar(max),
		destCallTerminationOnBehalfOf					varchar(max),
		origCalledPartyRedirectOnBehalfOf				varchar(max),
		lastRedirectRedirectOnBehalfOf					varchar(max),
		origCalledPartyRedirectReason					varchar(max),
		lastRedirectRedirectReason						varchar(max),
		destConversationId								varchar(max),
		globalCallId_ClusterID							varchar(max),
		joinOnBehalfOf									varchar(max),
		comment											varchar(max),
		authCodeDescription								varchar(max),
		authorizationLevel								varchar(max),
		clientMatterCode								varchar(max),
		origDTMFMethod									varchar(max),
		destDTMFMethod									varchar(max),
		callSecuredStatus								varchar(max),
		origConversationId								varchar(max),
		origMediaCap_Bandwidth							varchar(max),
		destMediaCap_Bandwidth							varchar(max),	
		authorizationCodeValue							varchar(max),
		outpulsedCallingPartyNumber						varchar(max),
		outpulsedCalledPartyNumber						varchar(max),
		origIpv4v6Addr									varchar(max),
		destIpv4v6Addr									varchar(max),
		origVideoCap_Codec_Channel2						varchar(max),
		origVideoCap_Bandwidth_Channel2					varchar(max),
		origVideoCap_Resolution_Channel2				varchar(max),
		origVideoTransportAddress_IP_Channel2			varchar(max),
		origVideoTransportAddress_Port_Channel2			varchar(max),
		origVideoChannel_Role_Channel2					varchar(max),
		destVideoCap_Codec_Channel2						varchar(max),
		destVideoCap_Bandwidth_Channel2					varchar(max),
		destVideoCap_Resolution_Channel2				varchar(max),
		destVideoTransportAddress_IP_Channel2			varchar(max),
		destVideoTransportAddress_Port_Channel2			varchar(max),
		destVideoChannel_Role_Channel2					varchar(max),
		incomingProtocolID								varchar(max),
		incomingProtocolCallRef							varchar(max),
		outgoingProtocolID								varchar(max),
		outgoingProtocolCallRef							varchar(max),
		currentRoutingReason							varchar(max),
		origRoutingReason								varchar(max),
		lastRedirectingRoutingReason					varchar(max),
		huntPilotDN										varchar(max),
		huntPilotPartition								varchar(max),
		calledPartyPatternUsage							varchar(max),
		outpulsedOriginalCalledPartyNumber				varchar(max),
		outpulsedLastRedirectingNumber					varchar(max)
	)

	-- Lee el archivo y lo inserta en la tabla temporal
	BULK
	INSERT #Temp
	--FROM '\\SRV15-ASP4\ArchivosAlestra\LA MODERNA Mayo 16 al 31.txt'--Ruta archivo
	FROM '@Direccion'--Ruta archivo
	WITH

	(DATAFILETYPE='char',
	Firstrow = 2,
	CODEPAGE = '1252',
	FIELDTERMINATOR = ',',
	ROWTERMINATOR = '0x0a') --separa filas


	select * from #Temp
	

	--Trunca tabla temporal
	--truncate table tab_calls_temp
	--select * from tab_calls_temp

	--Hace insert en tabla tab_calls_temp
    
	insert  into tab_calls_temp ( dateTimeOrigination, callingPartyNumber, originalCalledPartyNumber, finalCalledPartyNumber, dateTimeConnect, dateTimeDisconnect, duration, authorizationCodeValue) 
	select	CAST(T.dateTimeOrigination AS float) as fecha, 
			CAST(T.callingPartyNumber AS nvarchar(100)) as callingParty, 
			CAST(T.originalCalledPartyNumber AS nvarchar(100)) as originalParty, 
			CAST(T.finalCalledPartyNumber AS nvarchar(100)) as finalParty, 
			CAST(T.dateTimeConnect AS float), CAST(T.dateTimeDisconnect AS float), CAST(T.duration AS float), 
			CAST(T.authorizationCodeValue AS nvarchar(20)) 
	FROM #Temp T
	WHERE	
			not exists(select dateTimeOrigination,callingPartyNumber,originalCalledPartyNumber,finalCalledPartyNumber from tab_calls_temp where
			dateTimeOrigination = CAST(T.dateTimeOrigination AS float)
			and  callingPartyNumber = T.callingPartyNumber
			and originalCalledPartyNumber = T.originalCalledPartyNumber
			and finalCalledPartyNumber = T.finalCalledPartyNumber)

			and cdrRecordType <> 'cdrRecordType' 
			and (T.originalCalledPartyNumber is not null 
			and T.dateTimeOrigination is not null 
			and T.callingPartyNumber is not null 
			and T.finalCalledPartyNumber is not null)
	


	--Actualiza Cisco
	--EXEC PASS1

	--UPDATE tab_calls_temp set tab_calls_temp.id_callType = tab_rates.prefix, tab_calls_temp.cost = tab_rates.costo
	--FROM tab_calls_temp, tab_rates 
	--WHERE tab_calls_temp.finalCalledPartyNumber like tab_rates.query and tab_calls_temp.id_company = tab_rates.id_company

	--EXEC PASS2

	--DROP TABLE #Temp
	
	

END

--EXEC SubirInformacion '\\SRV15-ASP4\ArchivosAlestra\LA MODERNA Mayo 16 al 31.txt'


