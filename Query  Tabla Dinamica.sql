SELECT        TOP (100) PERCENT S_BoletasDeCalidad.noboleta AS entrada, C_Boletas.noboleta AS boleta, CONVERT(varchar(10), C_Boletas.fechapeso1, 120) AS fchemi, S_BoletasDeCalidad.noproveedor AS productor, 
                         S_ClientesProveedores.nombre AS nombre1, S_ClientesProveedores.apellidopaterno + ' ' + S_ClientesProveedores.apellidomaterno AS nombre2, S_Contratos.nocontrato AS contrato, 
                         S_BoletasDeCalidad.noubicacion AS creceptor, S_Ubicaciones.descripcion AS nombcr, C_Boletas.peso1 AS pesobruto, C_Boletas.peso2 AS pesotara, CAST(C_Boletas.peso1 AS decimal) 
                         - CAST(C_Boletas.peso2 AS decimal) AS pesoneto, S_Ejidos.noejido AS cveeji, S_Ejidos.ejido AS nombeji, S_Ejidos.nomunicipio AS cvemun, S_Ejidos.noestado AS municipio, S_Ejidos.noestado AS cveedo, 
                         S_Estados.estado, S_BoletasDeCalidad.chofer, S_BoletasDeCalidad.placas, S_BoletasDeCalidad.marca, S_BoletasDeCalidad.modelo, S_BoletasDeCalidad.color, S_BoletasDeCalidad.nograno AS tipograno, 
                         5 AS grupo, S_BoletasDeCalidad.novariedad AS variedad,
                             (SELECT        Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos ON Salmantina_301.dbo.Tab_NormasDeCalidad.nonorma = Salmantina_301.dbo.Tab_RelacionesNormasGranos.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos ON Salmantina_301.dbo.Tab_RelacionesNormasGranos.nograno = Salmantina_301.dbo.Tab_Granos.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad ON 
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos.norelacion = Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad.norelacion
                               WHERE        (Salmantina_301.dbo.Tab_NormasDeCalidad.norma LIKE N'%Impurezas%') AND (Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Salmantina_301.dbo.Tab_Granos.nograno = S_BoletasDeCalidad.nograno)) AS impurezas,
                             (SELECT        Tab_NormasDeBoletasDeCalidad_2.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON 
                                                         Tab_RelacionesNormasGranos_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Peso Especifico%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) AS pesoespecifico,
                             (SELECT        Tab_NormasDeBoletasDeCalidad_1.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_1 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_1 ON Tab_NormasDeCalidad_1.nonorma = Tab_RelacionesNormasGranos_1.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_1 ON Tab_RelacionesNormasGranos_1.nograno = Tab_Granos_1.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_1 ON 
                                                         Tab_RelacionesNormasGranos_1.norelacion = Tab_NormasDeBoletasDeCalidad_1.norelacion
                               WHERE        (Tab_NormasDeCalidad_1.norma LIKE N'%Humedad%') AND (Tab_NormasDeBoletasDeCalidad_1.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_1.nograno = S_BoletasDeCalidad.nograno)) AS humedad, 0 AS temp,
                             (SELECT        Tab_NormasDeBoletasDeCalidad_2.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON 
                                                         Tab_RelacionesNormasGranos_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Panza Blanca%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) AS panzablanca, 0 AS puntanegra,
                             (SELECT        Tab_NormasDeBoletasDeCalidad_2.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON 
                                                         Tab_RelacionesNormasGranos_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Grano Da�ado%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) AS granodanado,
                             (SELECT        Tab_NormasDeBoletasDeCalidad_2.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON 
                                                         Tab_RelacionesNormasGranos_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Grano Verde%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) AS granoverde, 0 AS granosquebrados, 0 AS otrvar, 0 AS estaquillas, 0 AS granosfogueados,
                             (SELECT        Tab_NormasDeBoletasDeCalidad_2.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON 
                                                         Tab_RelacionesNormasGranos_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Grano Picado%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) AS granopicado, 0 AS carbonparcial,
                             (SELECT        Tab_NormasDeBoletasDeCalidad_2.valor
                               FROM            Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON 
                                                         Tab_RelacionesNormasGranos_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Proteina%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) AS proteina, 0 AS inspfisica, 0 AS condfisicas, 0 AS condsanit, 0 AS aceprech, 0 AS procedencia, 0 AS usuario, 0 AS fchmov, 0 AS hrmov,
                             
							 
							 (SELECT        COALESCE (Salmantina_301.dbo.Tab_TablasDeCalidad.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Salmantina_301.dbo.Tab_TablasDeCalidad.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND
                                                          Salmantina_301.dbo.Tab_TablasDeCalidad.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Humed%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kghume,
                             
							 
							 (SELECT        COALESCE (Tab_TablasDeCalidad_1.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_1 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_1.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_1.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Impurezas%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kgimpu,
                             
							 
							 (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Peso Espec%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kgpesesp, (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Grano Quebrado%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kggrqueb,  (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Fogueado%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kggrfog, 
                         (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Carbon Parcial%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kgcarbpar, (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Proteina%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kggpo, 0 AS kgpanbla,



                             (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Impurezas%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kggrdan,
														 
														 
														 
														 
														 
														 
														 
														  (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Grano Verde%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kggrverde, (SELECT        COALESCE (Tab_TablasDeCalidad_2.acastigar, 0) AS Expr1
                               FROM            Salmantina_301.dbo.Tab_TablasDeCalidad AS Tab_TablasDeCalidad_2 RIGHT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeBoletasDeCalidad AS Tab_NormasDeBoletasDeCalidad_2 ON Tab_TablasDeCalidad_2.valor = Tab_NormasDeBoletasDeCalidad_2.valor AND 
                                                         Tab_TablasDeCalidad_2.norelacion = Tab_NormasDeBoletasDeCalidad_2.norelacion LEFT OUTER JOIN
                                                         Salmantina_301.dbo.Tab_NormasDeCalidad AS Tab_NormasDeCalidad_2 INNER JOIN
                                                         Salmantina_301.dbo.Tab_RelacionesNormasGranos AS Tab_RelacionesNormasGranos_2 ON Tab_NormasDeCalidad_2.nonorma = Tab_RelacionesNormasGranos_2.nonorma INNER JOIN
                                                         Salmantina_301.dbo.Tab_Granos AS Tab_Granos_2 ON Tab_RelacionesNormasGranos_2.nograno = Tab_Granos_2.nograno ON 
                                                         Tab_NormasDeBoletasDeCalidad_2.norelacion = Tab_RelacionesNormasGranos_2.norelacion
                               WHERE        (Tab_NormasDeCalidad_2.norma LIKE N'%Punta Negra%') AND (Tab_NormasDeBoletasDeCalidad_2.noboleta = S_BoletasDeCalidad.noboleta) AND 
                                                         (Tab_Granos_2.nograno = S_BoletasDeCalidad.nograno)) * (CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal)) / 1000 AS kgpuntaneg, 
                         0 AS kgestaq, 0 AS kggrpicado, 0 AS aplicocastigos, 0 AS kiloscastigos, 0 AS retenpredial, 0 AS preciograno, 0 AS subtotal, 0 AS grantotal, 0 AS entrpagada, 0 AS cheque, 0 AS fchcheq, 0 AS cancelado, 
                         CAST(C_Boletas.peso1 AS decimal) - CAST(C_Boletas.peso2 AS decimal) AS pesonetoanalizado, 0 AS seleccionado, 0 AS procesado, 0 AS factura, 0 AS importecontab, 0 AS noliqusincheque, 0 AS aplicoacumentr, 
                         0 AS nombtg, C_Bodegas.nobodega AS silodestino, C_Bodegas.descripcion AS nombresilo, C_Boletas.fechapeso1
FROM            Salmantina_301.dbo.Tab_Contratos AS S_Contratos RIGHT OUTER JOIN
                         Salmantina_301.dbo.Tab_Ubicaciones AS S_Ubicaciones RIGHT OUTER JOIN
                         Salmantina_301.dbo.Tab_Ejidos AS S_Ejidos LEFT OUTER JOIN
                         Salmantina_301.dbo.Tab_Estados AS S_Estados ON S_Ejidos.noestado = S_Estados.noestado RIGHT OUTER JOIN
                         Salmantina_301.dbo.Tab_BoletasDeCalidad AS S_BoletasDeCalidad RIGHT OUTER JOIN
                         dbo.Tab_BoletasSistemasExternos AS C_BoletasSistemasExternos RIGHT OUTER JOIN
                         dbo.Tab_Boletas AS C_Boletas ON C_BoletasSistemasExternos.nosistema = C_Boletas.sistemaexterno AND C_BoletasSistemasExternos.noticket = C_Boletas.noboleta AND 
                         C_BoletasSistemasExternos.noalmacen = C_Boletas.noalmacen ON S_BoletasDeCalidad.noboleta = C_BoletasSistemasExternos.refext LEFT OUTER JOIN
                         dbo.Tab_Bodegas AS C_Bodegas ON C_BoletasSistemasExternos.nobodega = C_Bodegas.nobodega ON S_Ejidos.noejido = S_BoletasDeCalidad.noejido ON 
                         S_Ubicaciones.noubicacion = S_BoletasDeCalidad.noubicacion LEFT OUTER JOIN
                         Salmantina_301.dbo.Tab_ClientesProveedores_TablasDinamicas AS S_ClientesProveedores ON S_BoletasDeCalidad.noproveedor = S_ClientesProveedores.no ON 
                         S_Contratos.nocontrato = S_BoletasDeCalidad.nograno
WHERE        (DATEPART(year, C_Boletas.fechapeso1) = DATEPART(year, GETDATE())) AND (CONVERT(varchar(10), C_Boletas.fechapeso1, 120) > '2016-04-12') AND (CAST(C_Boletas.peso1 AS decimal) 
                         > CAST(C_Boletas.peso2 AS decimal))