
declare @trab varchar(10)
declare @cia varchar(5)
declare @idcalendario int

set @trab =	'     26164'
set @cia = 'MOFE'
SET @idcalendario = '19843'

--select * from vw_dis_recibos where trabajador <> '     22856'
--select * from vw_dis_recibos where trabajador = @trab
--select * from rel_conceptos_agr where concepto = 18
--insert into rel_conceptos_agr select 'W12', compania, concepto, signo from rel_conceptos_agr where concepto = 18 and compania = 'MOFE'
--delete from vw_dis_recibos where trabajador <> '     22856'

--// PERCEPCIONES EXCENTAS
SELECT        VW.concepto as Percepciones_Excentas, RC.agr_conceptos, VW.nom_concepto, VW.importe, CASE WHEN
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 1) IS NULL THEN '' ELSE
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 1) END AS clave
FROM            vw_dis_recibos AS VW INNER JOIN
                         rel_conceptos_agr AS RC ON VW.concepto = RC.concepto
WHERE        (VW.trabajador = @trab) AND (VW.tipo <> 2) AND (RC.compania = @CIA) AND (RC.agr_conceptos = 'W12') 
AND (VW.idcalendario = @idcalendario)


--// Percepciones Fondo de Ahorro Excento
SELECT        TOP (1) 722 AS concepto, 'Fondo Ahorro Empresa' AS nom_concepto, CASE WHEN
			 (SELECT        SUM(importe) AS Expr1
              FROM            vw_dis_recibos
              WHERE        (concepto = 730) AND (trabajador = VW.trabajador)) - (CASE WHEN
              (SELECT        SUM(importe) AS Expr1
              FROM            vw_dis_recibos AS vw_dis_recibos_1
              WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) IS NULL THEN 0 ELSE
              (SELECT        SUM(importe) AS Expr1
              FROM            vw_dis_recibos AS vw_dis_recibos_1
              WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) END) IS NULL THEN '0' ELSE
              (SELECT        SUM(importe) AS Expr1
              FROM            vw_dis_recibos
              WHERE        (concepto = 730) AND (trabajador = VW.trabajador)) - CASE WHEN
              (SELECT        SUM(importe) AS Expr1
              FROM            vw_dis_recibos AS vw_dis_recibos_1
              WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) IS NULL THEN 0 ELSE
              (SELECT        SUM(importe) AS Expr1
              FROM            vw_dis_recibos AS vw_dis_recibos_1
              WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) END END AS importe, 
			  '005' AS clave
FROM            vw_dis_recibos AS VW
WHERE        (trabajador = @trab)  AND (compania = @cia) AND (idcalendario = @idcalendario)



--// Percepciones Gravadas
SELECT        VW.concepto as Percepciones_gravadas, VW.nom_concepto, VW.importe, CASE WHEN
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 1) IS NULL THEN '' ELSE
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 1) END AS clave
FROM            vw_dis_recibos AS VW INNER JOIN
                         rel_conceptos_agr AS RC ON VW.concepto = RC.concepto
WHERE        (VW.trabajador = @trab) AND (VW.tipo <> 2) AND (RC.compania = @CIA) AND (RC.agr_conceptos = 'W11') 
AND (VW.idcalendario = @idcalendario)





--select * from conceptos

select * from vw_dis_recibos where trabajador = @trab and idcalendario = @idcalendario 


select * from vw_dis_recibos WHERE trabajador = 22856
select * from trans_COME_19606 WHERE trabajador = 22856

select * from vw_dis_recibos where nom_concepto like '%dev%'

and (concepto='722' or concepto = '255'  or concepto = '256'  or concepto = '725') order by concepto

--// Deducciones gravadas
SELECT VW.concepto, VW.nom_concepto, VW.importe, 
CASE WHEN (SELECT TOP 1 X.clave FROM rpt_SAT_cat_PDI X WHERE X.concepto = VW.concepto AND X.TIpo = 2) IS NULL THEN '' 
ELSE (SELECT TOP 1 X.clave FROM rpt_SAT_cat_PDI X WHERE X.concepto = VW.concepto AND X.TIpo = 2) END AS clave 
FROM vw_dis_recibos AS VW INNER JOIN rel_conceptos_agr AS RC ON VW.concepto = RC.concepto 
WHERE (VW.trabajador = @trab) AND (VW.tipo <> 1) AND (RC.compania = @CIA) 
AND (RC.agr_conceptos = 'DED') 
AND (VW.idcalendario = @idcalendario)

--//Deducciones excentas
SELECT TOP (1) 722 AS concepto, 'Fondo Ahorro Empresa' AS nom_concepto, 
CASE WHEN (SELECT SUM(importe) AS Expr1 FROM vw_dis_recibos WHERE (concepto = 730) 
AND (trabajador = VW.trabajador)) - (CASE WHEN (SELECT SUM(importe) AS Expr1 
FROM vw_dis_recibos AS vw_dis_recibos_1 
WHERE (concepto IN (255 , 256 , 725)) AND (trabajador = VW.trabajador)) IS NULL THEN 0 
ELSE (SELECT SUM(importe) AS Expr1 FROM vw_dis_recibos AS vw_dis_recibos_1 
WHERE (concepto IN (255 , 256 , 725)) AND (trabajador = VW.trabajador)) END) IS NULL THEN '0' 
ELSE (SELECT SUM(importe) AS Expr1 FROM vw_dis_recibos WHERE (concepto = 730) 
AND (trabajador = VW.trabajador)) - CASE WHEN (SELECT SUM(importe) AS Expr1 
FROM vw_dis_recibos AS vw_dis_recibos_1 WHERE (concepto IN (255 , 256 , 725)) 
AND (trabajador = VW.trabajador)) IS NULL THEN 0 ELSE (SELECT SUM(importe) AS Expr1 
FROM vw_dis_recibos AS vw_dis_recibos_1 WHERE (concepto IN (255 , 256 , 725)) 
AND (trabajador = VW.trabajador)) END END AS importe, '004' AS clave FROM vw_dis_recibos AS VW 
WHERE (trabajador = @TRAB) AND (idcalendario = @idcalendario)

--// Deducciones EX
SELECT        VW.concepto, VW.nom_concepto, VW.importe, CASE WHEN
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 2) IS NULL THEN '' ELSE
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 2) END AS clave
FROM            vw_dis_recibos AS VW INNER JOIN
                         rel_conceptos_agr AS RC ON VW.concepto = RC.concepto
WHERE        (VW.trabajador = @trab) AND (VW.tipo <> 1) AND (RC.compania = @CIA) AND (RC.agr_conceptos = 'DEX') 
and idcalendario = @idcalendario


select * from vw_dis_recibos where trabajador = @trab









--/

--SELECT * FROM rpt_SAT_cat_PDI WHERE concepto = '730'


select * from rpt_cons_cfdi where trabajador = @trab and compania = @cia

select * from vw_dis_recibos where trabajador = @trab and idcalendario = @idcalendario order by concepto
--select * from rel_conceptos_agr where agr_conceptos = 'W12' and compania = 'MOFE' AND concepto = '812'


--/*
SELECT VW.concepto, VW.nom_concepto, VW.importe, 
CASE WHEN (SELECT TOP 1 X.clave FROM rpt_SAT_cat_PDI X WHERE X.concepto = VW.concepto AND X.TIpo = 1) IS NULL THEN '' 
ELSE (SELECT TOP 1 X.clave FROM rpt_SAT_cat_PDI X WHERE X.concepto = VW.concepto AND X.TIpo = 1) END AS clave 
FROM vw_dis_recibos AS VW 
INNER JOIN rel_conceptos_agr AS RC 
ON VW.concepto = RC.concepto 
WHERE (VW.trabajador = @trab) 
AND (VW.tipo <> 2) AND 
(RC.compania = @CIA) AND 
(RC.agr_conceptos = 'W11')
AND (VW.idcalendario = @idcalendario)
--*/
--/*
SELECT VW.concepto, VW.nom_concepto, VW.importe, 
CASE WHEN (SELECT TOP 1 X.clave FROM rpt_SAT_cat_PDI X WHERE X.concepto = VW.concepto AND X.TIpo = 2) IS NULL THEN '' 
ELSE (SELECT TOP 1 X.clave FROM rpt_SAT_cat_PDI X WHERE X.concepto = VW.concepto AND X.TIpo = 2) END AS clave 
FROM vw_dis_recibos AS VW INNER JOIN rel_conceptos_agr AS RC ON VW.concepto = RC.concepto 
WHERE (VW.trabajador = @trab) 
AND (VW.tipo <> 1) 
AND (RC.compania = @CIA) 
AND (RC.agr_conceptos = 'DED') 
AND (VW.idcalendario = @idcalendario)
--*/

/*
NOD1   001 680            Cuota I.M.S.S.                                                                                      418.64            0.00              
NOD2   002 512            Impuesto antes del subsidio para empleo                                                             2720.46           0.00              
NOD3   004 256            Fondo de Ahorro Gravable                                                                            270.77            0.00              
NOD4   004 999            Redondeo Actual                                                                                     -0.01             0.00              
NOD5   004 722            Fondo Ahorro Empresa                                                                                0.00              1329.33           
NOD6   004 720            Aportacion Fondo de Ahorro Trabajador     
*/
--/*
SELECT        TOP (1) 722 AS concepto, 'Fondo Ahorro Empresa' AS nom_concepto, CASE WHEN
                             (SELECT        SUM(importe) AS Expr1
                               FROM            vw_dis_recibos
                               WHERE        (concepto = 730) AND (trabajador = VW.trabajador)) - (CASE WHEN
                             (SELECT        SUM(importe) AS Expr1
                               FROM            vw_dis_recibos AS vw_dis_recibos_1
                               WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) IS NULL THEN 0 ELSE
                             (SELECT        SUM(importe) AS Expr1
                               FROM            vw_dis_recibos AS vw_dis_recibos_1
                               WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) END) IS NULL THEN '0' ELSE
                             (SELECT        SUM(importe) AS Expr1
                               FROM            vw_dis_recibos
                               WHERE        (concepto = 730) AND (trabajador = VW.trabajador)) - CASE WHEN
                             (SELECT        SUM(importe) AS Expr1
                               FROM            vw_dis_recibos AS vw_dis_recibos_1
                               WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) IS NULL THEN 0 ELSE
                             (SELECT        SUM(importe) AS Expr1
                               FROM            vw_dis_recibos AS vw_dis_recibos_1
                               WHERE        (concepto IN (255, 256, 725)) AND (trabajador = VW.trabajador)) END END AS importe, '004' AS clave
FROM            vw_dis_recibos AS VW
WHERE        (trabajador = @TRAB) AND (idcalendario = @idcalendario)
--*/

SELECT        VW.concepto, VW.nom_concepto, VW.importe, CASE WHEN
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 2) IS NULL THEN '' ELSE
                             (SELECT        TOP 1 X.clave
                               FROM            rpt_SAT_cat_PDI X
                               WHERE        X.concepto = VW.concepto AND X.TIpo = 2) END AS clave
FROM            vw_dis_recibos AS VW INNER JOIN
                         rel_conceptos_agr AS RC ON VW.concepto = RC.concepto
WHERE        (VW.trabajador = @trab) AND (VW.tipo <> 1) AND (RC.compania = @CIA) AND (RC.agr_conceptos = 'DEX') 
AND (VW.idcalendario = @idcalendario)

--/*
select * from rpt_SAT_cat_PDI  where concepto = '256'
select * from rpt_SAT_cat_PDI  where concepto = '730'

--DELETE from rpt_SAT_cat_PDI  where concepto = '256' AND TIPO = 1

--*/--

--select * from rel_conceptos_agr where agr_conceptos = 'W11' and compania = 'PEPL' and concepto = '730'
--delete from rel_conceptos_agr where agr_conceptos = 'W11' and compania = 'PEPL' and concepto = '730'
--
--insert into rpt_SAT_cat_PDI
--select clave, '730', tipo from rpt_SAT_cat_PDI where concepto = '256' and tipo = 1
--

select * from rpt_SAT_cat_PDI where concepto = 812

select * from rel_conceptos_agr where agr_conceptos = 'W12' and compania = 'MOFE' AND concepto = '812'

--insert into rel_conceptos_agr 
select agr_conceptos, compania, '812', signo from rel_conceptos_agr where agr_conceptos = 'W12' and compania = 'TRNA' AND concepto = '3093'
--select * from rel_conceptos_agr where agr_conceptos = 'W12' and compania = 'PEPL' AND concepto = '142'

select * from rel_conceptos_agr where compania = 'TRNA'

select * from rel_conceptos_agr where agr_conceptos = 'W12' and compania = 'MOFE' AND concepto = '256'
--delete from rel_conceptos_agr where agr_conceptos = 'W12' and compania = 'MOFE' AND concepto = '256'

select * from rel_conceptos_agr where agr_conceptos = 'W11' and compania = 'MOFE' AND concepto = '256'


select * from rel_conceptos_agr where concepto = '722'

delete from rel_conceptos_agr where concepto = '722'


select * from rpt_cons_cfdi where folio = 'CO00001911'



select * from rpt_cons_cfdi where clase_nomina = 'PM' and tipo_nomina = 'PM' and anio = 2016 and periodo = 4 order by folio

--delete from rpt_cons_cfdi where folio = 'PM00003942'

update rpt_cons_cfdi set folio = 'PM00003943' where folio = 'PM00003942'

--update rpt_cons_cfdi set conse = 3943 where folio = 'PM00003943'


select * from rpt_cons_cfdi where folio > 'PM00003876' and clase_nomina = 'PM'

delete from rpt_cons_cfdi where folio between 'PM00003876' and 'PM00003942'