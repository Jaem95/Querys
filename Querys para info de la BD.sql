Select * FROM sys.fn_dblog(NULL,NULL)


USE NSMixtoSemanal;
GO
-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE NSMixtoSemanal
SET RECOVERY SIMPLE;
GO
-- Shrink the truncated log file to 1 MB.
DBCC SHRINKFILE (NSMixtoSemanal_log, 1);
GO
-- Reset the database recovery model.
ALTER DATABASE NSMixtoSemanal
SET RECOVERY FULL;
GO

DBCC UPDATEUSAGE ('NSMixtoSemanal')


USE NSMixtoSemanal;  
GO  
EXEC sp_spaceused N'Cias';  
GO  



DBCC SHRINKDATABASE (NSMixtoSemanal, 10);
GO


DBCC SQLPERF(LOGSPACE)

